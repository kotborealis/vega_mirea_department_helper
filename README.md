[![pipeline status](https://gitlab.com/mixa2130/vega_mirea_department_helper/badges/master/pipeline.svg)](https://gitlab.com/mixa2130/vega_mirea_department_helper/-/commits/master)

# vega_mirea_department_helper

Комплекс приложений разработанных для концерна "Вега".

Данный проект базируется на основе разработанного ранее [vega_echo_bot](https://gitlab.com/mixa2130/vega_echo_bot)
## Installation

### Подготовка

Перед запуском docker-compose необходимо: 
* Ознакомиться с README каждого python пакета из корня проекта;
* Произвести соответствующую конфигурацию в соответствии с инструкциями, данными в файлах README.

### Docker

___Сборка образов:___
```sh
docker-compose build
```

___Запуск контейнеров___:

```sh
docker-compose up
```

___Просмотр логов___

```sh
docker-compose logs -f [service name]
```

___Выполнить команду в выполняющемся контейнере___

```sh
docker-compose exec [service name] [command]
```
