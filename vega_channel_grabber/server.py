"""
Telegram application for receiving data published in telegram channels.
Based on API telegram client.
"""
import os
import logging.config

from dotenv import load_dotenv
from telethon import TelegramClient, events

# Logging
logging.config.fileConfig(fname='logs.conf', disable_existing_loggers=True)
LOGGER = logging.getLogger('channel_grabber.server')

# Configuration
load_dotenv()

API_ID: int = int(os.getenv('API_ID'))
API_HASH: str = os.getenv('API_HASH')
TARGET_CHANNEL = int(os.getenv('TARGET_CHANNEL'))

_source_channels: str = os.getenv('CHANNELS')  # "test_channel1_id, test_channel2_id..."
CHANNELS: list = _source_channels.split(',')
for index, _raw_channel in enumerate(CHANNELS):
    CHANNELS[index] = int(_raw_channel)

client = TelegramClient('grabber', API_ID, API_HASH)


@client.on(events.NewMessage(chats=CHANNELS))
@client.on(events.MessageEdited(chats=CHANNELS))
async def grab_msg_from_channel(event):
    LOGGER.debug('New message in source channel')

    await client.forward_messages(TARGET_CHANNEL, event.message)
    LOGGER.debug('Message forwarded to target channel')


if __name__ == '__main__':
    client.start()
    LOGGER.info('Authorization complete. Channel grabber started')

    try:
        client.run_until_disconnected()
    except Exception as exc:
        LOGGER.error(repr(exc))

    LOGGER.info('Channel grabber ended')
