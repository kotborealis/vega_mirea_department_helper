"""TG channel grabber - server part"""
import os
import logging.config

import aiohttp
from dotenv import load_dotenv
from aiogram import Dispatcher, Bot, types
from aiogram.utils.executor import start_webhook
from handler import DataSync

# Configure logging
logging.config.fileConfig(fname='logs.conf', disable_existing_loggers=True)
logger = logging.getLogger('echo_bot.server')

logger.info('Bot started')
load_dotenv()

API_TOKEN = os.getenv('TG_BOT_API_TOKEN')

# Webhook settings
WEBHOOK_HOST = os.getenv('WEBHOOK_HOST')
WEBHOOK_PATH = os.getenv('WEBHOOK_PATH')
WEBHOOK_URL = os.getenv('WEBHOOK_URL')

# Webserver settings
WEBAPP_HOST = os.getenv('WEBAPP_HOST')
WEBAPP_PORT = os.getenv('WEBAPP_PORT')

# REST API
REST_URL = os.getenv('REST_URL')
REST_ENDPOINT = os.getenv('REST_ENDPOINT')
REST_TOKEN = os.getenv('REST_TOKEN')  # auth token, which is transmitted as a json field in an output structure
REST_URI = f"{REST_URL}{REST_ENDPOINT}"

bot = Bot(API_TOKEN)
dp = Dispatcher(bot)


async def rest_api(json_data: dict):
    """
    Responsible for sending data to the server using REST API.
    REST_URI is built on REST_URL and REST_ENDPOINT from .env.
    """
    json_data['REST_TOKEN'] = REST_TOKEN

    async with aiohttp.ClientSession() as session:
        try:
            async with session.post(REST_URI, json=json_data, headers={'Content-Encoding': 'UTF-8'}) as response:
                status = response.status
                logger.info('Sending status: %s', status)

                if status // 100 in (4, 5):
                    logger.error('HTTP error: %s - %s', status, response.reason)

        except Exception as exc:
            logger.error(repr(exc))


@dp.channel_post_handler(content_types=types.ContentType.PHOTO)
@dp.edited_channel_post_handler(content_types=types.ContentType.PHOTO)
async def photo_in_channel_post(message: types.Message):
    """
    Channel post, which contains a photo - was sent or edited
    """
    logger.debug('сообщение с фото пришло на обработку')

    from_msg = DataSync('photo', message)
    validate_data = from_msg.to_dict()

    logger.debug('сообщение было обработано и переходит в метод отправки на сервер')
    await rest_api(validate_data)


@dp.channel_post_handler(content_types=types.ContentType.TEXT)
@dp.edited_channel_post_handler(content_types=types.ContentType.TEXT)
async def text_in_channel_post(message: types.Message):
    """
    Channel post, which is a text message - was sent or edited
    """
    logger.debug('текстовое сообщение пришло на обработку')

    from_msg = DataSync('text', message)
    validate_data = from_msg.to_dict()

    logger.debug('сообщение было обработано и переходит в метод отправки на сервер')
    await rest_api(validate_data)


@dp.channel_post_handler(content_types=types.ContentType.DOCUMENT)
@dp.edited_channel_post_handler(content_types=types.ContentType.DOCUMENT)
async def document_in_channel_post(message: types.Message):
    """
    Channel post, which contains a document - was sent or edited
    """
    logger.debug('сообщение с документом пришло на обработку')

    from_msg = DataSync('document', message)
    validate_data = from_msg.to_dict()

    logger.debug('сообщение было обработано и переходит в метод отправки на сервер')
    await rest_api(validate_data)


@dp.channel_post_handler(content_types=types.ContentType.VOICE)
@dp.edited_channel_post_handler(content_types=types.ContentType.VOICE)
async def voice_in_channel_post(message: types.Message):
    """
    Channel post, which contains a voice message - was sent or edited
    """
    logger.debug('голосовое сообщение пришло на обработку')

    from_msg = DataSync('voice', message)
    validate_data = from_msg.to_dict()

    logger.debug('сообщение было обработано и переходит в метод отправки на сервер')
    await rest_api(validate_data)


@dp.channel_post_handler(content_types=types.ContentType.PINNED_MESSAGE)
async def pinned_msg_in_channel_post(message: types.Message):
    """
    Channel post has been pinned
    """
    logger.debug('сообщение было закреплено')

    from_msg = DataSync.from_pinned_msg(message)

    if from_msg is not None:
        validate_data = from_msg.to_dict()

        logger.debug('сообщение было обработано и переходит в метод отправки на сервер')
        await rest_api(validate_data)
    else:
        logger.info('Неподдерживаемый формат закреплённого сообщения')


async def on_startup(dispatcher):
    """
    will be executed at the project startup
    """
    # To avoid problems with connection after Exception
    await bot.delete_webhook()
    logger.info('old webhook has been deleted')

    await bot.set_webhook(WEBHOOK_URL)
    logger.info('new webhook has started')


async def on_shutdown(dispatcher):
    """
    Will be executed at the project shutdown
    """
    await bot.delete_webhook()
    logger.info('Bot ended')


if __name__ == '__main__':
    start_webhook(dispatcher=dp,
                  webhook_path=WEBHOOK_PATH,
                  on_startup=on_startup,
                  on_shutdown=on_shutdown,
                  skip_updates=True,
                  host=WEBAPP_HOST,
                  port=WEBAPP_PORT)
