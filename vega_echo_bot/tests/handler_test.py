import unittest

from unittest.mock import patch
from datetime import datetime
from handler import DataSync


class TestClassDataSync(unittest.TestCase):
    def test_DataSync_init_photo(self):
        photo_msg_resp = {"message_id": 137, "author_signature": "Test",
                          "sender_chat": {"id": -1001162, "title": "test", "type": "channel"},
                          "chat": {"id": -1001162310, "title": "test", "type": "channel"},
                          "date": datetime.fromtimestamp(1614360006),
                          "photo": [
                              {
                                  "file_id": "AgACAgIAAx0CRUd1QwADiWA5LcbHIuYNxnNdCS",
                                  "file_unique_id": "AQADzXEXmy4AA42uAgAB",
                                  "file_size": 16390, "width": 320, "height": 253},
                              {
                                  "file_id": "AgACAgIAAx0CRUd1QwADiWA5LcbHIuYNxnNdCSiV",
                                  "file_unique_id": "AQADzXEXmy4AA46uAgAB",
                                  "file_size": 68144, "width": 800, "height": 632},
                              {
                                  "file_id": "AgACAgIAAx0CRUd1QwADiWA5LcbHIuYNxnNdCSiVae",
                                  "file_unique_id": "AQADzXEXmy4AA4yuAgAB",
                                  "file_size": 73585, "width": 960, "height": 759}],
                          'caption': 'Oh my gosh!'}

        photo_class = DataSync('photo', photo_msg_resp)
        self.assertEqual(photo_class.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310,
                                 "title": "test",
                                 "type": "channel"
                             },
                             'edited': False, 'pinned_message': False,
                             'message_id': 137, 'author_nickname': 'Test',
                             'text': 'Oh my gosh!',
                             'date': 1614360006.0,
                             'file': {
                                 'file_id': 'AgACAgIAAx0CRUd1QwADiWA5LcbHIuYNxnNdCSiVae',
                                 'file_type': 'photo',
                                 'file_size': 73585}
                         })

        # Message was edited
        edited_photo_msg_resp = {"message_id": 147, "author_signature": "Test",
                                 "sender_chat": {"id": -100116231, "title": "test", "type": "channel"},
                                 "chat": {"id": -100116231, "title": "test", "type": "channel"},
                                 "date": 1614367720, "edit_date": datetime.fromtimestamp(1614367724),
                                 "photo": [{"file_id": "AgACAgIAAx0CRUd1QwADk2A5S-zXgwHYcj",
                                            "file_unique_id": "AQADL1YPni4AA8iuAgAB",
                                            "file_size": 4243,
                                            "width": 320,
                                            "height": 114},
                                           {
                                               "file_id": "AgACAgIAAx0CRUd1QwADk2A5S-zXgwHYcj6",
                                               "file_unique_id": "AQADL1YPni4AA8auAgAB",
                                               "file_size": 12247,
                                               "width": 724,
                                               "height": 257}],
                                 "caption": "I'm leaving practice on VEGA soon("}

        edited_photo_class = DataSync('photo', edited_photo_msg_resp)
        self.assertEqual(edited_photo_class.to_dict(),
                         {
                             "chat":
                                 {"id": -100116231,
                                  "title": "test",
                                  "type": "channel"
                                  },
                             'edited': True, 'pinned_message': False,
                             'message_id': 147, 'author_nickname': 'Test',
                             'text': "I'm leaving practice on VEGA soon(",
                             'date': 1614367724.0,
                             'file': {
                                 'file_id': 'AgACAgIAAx0CRUd1QwADk2A5S-zXgwHYcj6',
                                 'file_type': 'photo',
                                 'file_size': 12247}
                         })

    def test_DataSync_init_text(self):
        text_msg_resp = {"message_id": 138, "author_signature": "Test",
                         "sender_chat": {"id": -100116231, "title": "test", "type": "channel"},
                         "chat": {"id": -100116231, "title": "test", "type": "channel"},
                         "date": datetime.fromtimestamp(1614362673), "text": "Homer!!"}

        text_class = DataSync('text', text_msg_resp)
        self.assertEqual(text_class.to_dict(),
                         {
                             "chat":
                                 {"id": -100116231,
                                  "title": "test",
                                  "type": "channel"
                                  },
                             'edited': False, 'pinned_message': False,
                             'message_id': 138, 'author_nickname': 'Test',
                             'text': 'Homer!!',
                             'date': 1614362673.0
                         })

        # Message was edited
        edited_text_msg_resp = {"message_id": 144, "author_signature": "Test",
                                "sender_chat": {"id": -100116231, "title": "test", "type": "channel"},
                                "chat": {"id": -1001162310, "title": "test", "type": "channel"},
                                "date": 1614366240, "edit_date": datetime.fromtimestamp(1614366886),
                                "text": "oh, homer!"}

        edited_text_class = DataSync('text', edited_text_msg_resp)
        self.assertEqual(edited_text_class.to_dict(),
                         {
                             "chat":
                                 {"id": -1001162310,
                                  "title": "test",
                                  "type": "channel"
                                  },
                             'edited': True, 'pinned_message': False,
                             'message_id': 144, 'author_nickname': 'Test',
                             'text': "oh, homer!",
                             'date': 1614366886.0
                         })

    def test_DataSync_init_doc_voice(self):
        # Voice message
        voice_msg_resp = {"message_id": 136, "author_signature": "Test",
                          "sender_chat": {"id": -1001162310979, "title": "test", "type": "channel"},
                          "chat": {"id": -1001162310979, "title": "test", "type": "channel"},
                          "date": datetime.fromtimestamp(1614351315), "voice": {"duration": 7, "mime_type": "audio/ogg",
                                                                                "file_id": "AwACAgIAAx0CRUd1QwADiGA5C9",
                                                                                "file_unique_id": "AgADpQ0AArm7yEk",
                                                                                "file_size": 46535}}

        voice_class = DataSync('voice', voice_msg_resp)
        self.assertEqual(voice_class.to_dict(),
                         {"chat":
                              {"id": -1001162310979,
                               "title": "test",
                               "type": "channel"
                               },
                          'edited': False, 'pinned_message': False,
                          'message_id': 136, 'author_nickname': 'Test',
                          'date': 1614351315.0,
                          'file':
                              {'file_id': 'AwACAgIAAx0CRUd1QwADiGA5C9',
                               'file_type': 'voice',
                               'file_size': 46535},
                          })

        # Document message
        doc_msg_resp = {"message_id": 135, "author_signature": "Test",
                        "sender_chat": {"id": -1001162, "title": "test", "type": "channel"},
                        "chat": {"id": -10011623, "title": "test", "type": "channel"},
                        "date": datetime.fromtimestamp(1614351262),
                        "document": {"file_name": "vygruzka_28_09_20 (2).xlsx",
                                     "mime_type": "application/vnd.sheet",
                                     "file_id": "BQACAgIAAx0CRUd1QwADh2A5C56x5bj4ny2qzpUq5x",
                                     "file_unique_id": "AgADow0AArm7yEk", "file_size": 6123},
                        'caption': 'Give me a donut!!'}

        doc_class = DataSync('document', doc_msg_resp)
        self.assertEqual(doc_class.to_dict(),
                         {"chat":
                              {"id": -10011623,
                               "title": "test",
                               "type": "channel"},
                          'edited': False, 'pinned_message': False,
                          'message_id': 135, 'author_nickname': 'Test',
                          'text': 'Give me a donut!!',
                          'date': 1614351262.0,
                          'file':
                              {'file_id': 'BQACAgIAAx0CRUd1QwADh2A5C56x5bj4ny2qzpUq5x',
                               'file_type': 'document',
                               'file_size': 6123,
                               "file_name": "vygruzka_28_09_20 (2).xlsx"}
                          })

        # Doc message was edited
        edited_doc_msg_resp = {"message_id": 148, "author_signature": "Test",
                               "sender_chat": {"id": -10011623, "title": "test", "type": "channel"},
                               "chat": {"id": -100116, "title": "test", "type": "channel"},
                               "date": 1614368881, "edit_date": datetime.fromtimestamp(1614368890),
                               "document": {"file_name": "university.txt", "mime_type": "text/plain",
                                            "file_id": "BQACAgIAAx0CRUd1QwADlGA5UHq7WQT0Kb5L",
                                            "file_unique_id": "AgADcw4AArm7yEk", "file_size": 9207},
                               "caption": "Привет"}

        edited_doc_class = DataSync('document', edited_doc_msg_resp)
        self.assertEqual(edited_doc_class.to_dict(),
                         {"chat":
                              {"id": -100116,
                               "title": "test",
                               "type": "channel"},
                          'edited': True, 'pinned_message': False,
                          'message_id': 148, 'author_nickname': 'Test',
                          'text': "Привет",
                          'date': 1614368890.0,
                          'file':
                              {'file_id': "BQACAgIAAx0CRUd1QwADlGA5UHq7WQT0Kb5L",
                               'file_type': 'document',
                               'file_size': 9207,
                               "file_name": "university.txt"},
                          })

    def test_DataSync_pinned_msg(self):
        text_msg_resp = {"message_id": 152,
                         "sender_chat": {"id": -1001, "title": "test", "type": "channel"},
                         "chat": {"id": -1009, "title": "test", "type": "channel"},
                         "date": datetime.fromtimestamp(1614373480),
                         "pinned_message": {"message_id": 144, "author_signature": "Test",
                                            "sender_chat": {"id": -1001,
                                                            "title": "test",
                                                            "type": "channel"},
                                            "chat": {"id": -10019, "title": "test",
                                                     "type": "channel"}, "date": 1614366240,
                                            "edit_date": 1614366886, "text": "oh, homer!"}}

        text_class = DataSync.from_pinned_msg(text_msg_resp)
        self.assertEqual(text_class.to_dict(), {
            "chat": {"id": -1009, "title": "test", "type": "channel"},
            'edited': False, 'pinned_message': True,
            'message_id': 144, 'author_nickname': 'Test',
            'text': "oh, homer!",
            'date': 1614373480.0})

        doc_msg_resp = {"message_id": 153,
                        "sender_chat": {"id": -10019, "title": "test", "type": "channel"},
                        "chat": {"id": -10011, "title": "test", "type": "channel"},
                        "date": datetime.fromtimestamp(1614373551),
                        "pinned_message": {"message_id": 140, "author_signature": "Test",
                                           "sender_chat": {"id": -10011, "title": "test",
                                                           "type": "channel"},
                                           "chat": {"id": -10011, "title": "test", "type": "channel"},
                                           "date": 1614363013, "document": {"file_name": "vygruzka_28_09_20 (2).xlsx",
                                                                            "mime_type": "application/...",
                                                                            "file_id": "BQACAgIAAx0CRUd1QwADjGA5Y",
                                                                            "file_unique_id": "AgADJw4AArm7yEk",
                                                                            "file_size": 6123},
                                           "caption": "Give me a donut!"}}

        doc_class = DataSync.from_pinned_msg(doc_msg_resp)
        self.assertEqual(doc_class.to_dict(), {
            "chat": {"id": -10011, "title": "test", "type": "channel"},
            'edited': False, 'pinned_message': True,
            'message_id': 140, 'author_nickname': 'Test',
            'text': "Give me a donut!",
            'file': {
                'file_id': "BQACAgIAAx0CRUd1QwADjGA5Y",
                'file_type': 'document',
                'file_size': 6123,
                'file_name': "vygruzka_28_09_20 (2).xlsx"},
            'date': 1614373551.0})

        photo_msg_resp = {"message_id": 151,
                          "sender_chat": {"id": -100, "title": "test", "type": "channel"},
                          "chat": {"id": -100, "title": "test", "type": "channel"},
                          "date": datetime.fromtimestamp(1614373334),
                          "pinned_message": {"message_id": 149, "author_signature": "Test",
                                             "sender_chat": {"id": -100,
                                                             "title": "test",
                                                             "type": "channel"},
                                             "chat": {"id": -100,
                                                      "title": "test", "type": "channel"},
                                             "date": 1614372406, "photo": [{
                                  "file_id": "AgACAgIAAx0CRUd1QwADlWA5XjZ9lC",
                                  "file_unique_id": "AQADKR4imy4AA4zBAwAB",
                                  "file_size": 6171,
                                  "width": 320,
                                  "height": 41}, {
                                  "file_id": "AgACAgIAAx0CRUd1QwADlWA5XjZ9lCB4rxGN",
                                  "file_unique_id": "AQADKR4imy4AA43BAwAB",
                                  "file_size": 25044,
                                  "width": 800,
                                  "height": 102}, {
                                  "file_id": "AgACAgIAAx0CRUd1QwADlWA5XjZ9lCB4rxG",
                                  "file_unique_id": "AQADKR4imy4AA4rBAwAB",
                                  "file_size": 29517,
                                  "width": 935,
                                  "height": 119}]}}

        photo_class = DataSync.from_pinned_msg(photo_msg_resp)
        self.assertEqual(photo_class.to_dict(), {
            "chat": {"id": -100, "title": "test", "type": "channel"},
            'edited': False, 'pinned_message': True,
            'message_id': 149, 'author_nickname': 'Test',
            'file':
                {
                    'file_id': "AgACAgIAAx0CRUd1QwADlWA5XjZ9lCB4rxG",
                    'file_type': 'photo',
                    'file_size': 29517},
            'date': 1614373334.0})

        voice_msg_resp = {"message_id": 154,
                          "sender_chat": {"id": -10011, "title": "test", "type": "channel"},
                          "chat": {"id": -100, "title": "test", "type": "channel"},
                          "date": datetime.fromtimestamp(1614373759),
                          "pinned_message": {"message_id": 141, "author_signature": "Test",
                                             "sender_chat": {"id": -100116979,
                                                             "title": "test",
                                                             "type": "channel"},
                                             "chat": {"id": -100116210979,
                                                      "title": "test", "type": "channel"},
                                             "date": 1614363086, "edit_date": 1614363092,
                                             "voice": {"duration": 2, "mime_type": "audio/ogg",
                                                       "file_id": "AwACAgIAAx0CRUd1QwADjWA5Y39r",
                                                       "file_unique_id": "AgADKA4AArm7yEk",
                                                       "file_size": 17010}, "caption": "xxxx"}}

        voice_class = DataSync.from_pinned_msg(voice_msg_resp)
        self.assertEqual(voice_class.to_dict(), {
            "chat": {"id": -100, "title": "test", "type": "channel"},
            'edited': False, 'pinned_message': True,
            'message_id': 141, 'author_nickname': 'Test',
            'text': "xxxx",
            'file': {
                'file_id': "AwACAgIAAx0CRUd1QwADjWA5Y39r",
                'file_type': 'voice',
                'file_size': 17010},
            'date': 1614373759.0})

    def test_DataSync_errors(self):
        msg_resp = {"message_id": 153,
                    "sender_chat": {"id": -10019, "title": "test", "type": "channel"},
                    "chat": {"id": -10011, "title": "test", "type": "channel"},
                    "date": datetime.fromtimestamp(1614373551),
                    "pinned_message": {"message_id": 140, "author_signature": "Test",
                                       "sender_chat": {"id": -10011, "title": "test",
                                                       "type": "channel"},
                                       "chat": {"id": -10011, "title": "test", "type": "channel"},
                                       "date": 1614363013, "document": {"file_name": "vygruzka_28_09_20 (2).xlsx",
                                                                        "mime_type": "application/...",
                                                                        "file_id": "BQACAgIAAx0CRUd1QwADjGA5Y",
                                                                        "file_unique_id": "AgADJw4AArm7yEk",
                                                                        "file_size": 6123},
                                       "caption": "Give me a donut!"}}

        with patch('handler.DataSync._file_info_handler', side_effect=KeyError):
            file_info_err = DataSync('document', msg_resp)

            self.assertEqual(file_info_err.to_dict(),
                             {'channel_id': -10011,
                              "error": "Message parser error, please check logs"})

        # Incorrect type
        file_info_err = DataSync('photo', msg_resp)

        self.assertEqual(file_info_err.to_dict(),
                         {'channel_id': -10011,
                          "error": "Message parser error, please check logs"})

    def test_DataSync_no_nickname(self):
        voice_msg_resp = {"message_id": 154,
                          "sender_chat": {"id": -10011, "title": "test", "type": "channel"},
                          "chat": {"id": -100, "title": "test", "type": "channel"},
                          "date": datetime.fromtimestamp(1614373759),
                          "pinned_message": {"message_id": 141,
                                             "sender_chat": {"id": -100116979,
                                                             "title": "test",
                                                             "type": "channel"},
                                             "chat": {"id": -100116210979,
                                                      "title": "test", "type": "channel"},
                                             "date": 1614363086, "edit_date": 1614363092,
                                             "voice": {"duration": 2, "mime_type": "audio/ogg",
                                                       "file_id": "AwACAgIAAx0CRUd1QwADjWA5Y39r",
                                                       "file_unique_id": "AgADKA4AArm7yEk",
                                                       "file_size": 17010}, "caption": "xxxx"}}

        voice_class = DataSync.from_pinned_msg(voice_msg_resp)
        self.assertEqual(voice_class.to_dict(), {
            "chat": {"id": -100, "title": "test", "type": "channel"},
            'edited': False, 'pinned_message': True,

            'message_id': 141,
            'text': "xxxx",
            'file': {
                'file_id': "AwACAgIAAx0CRUd1QwADjWA5Y39r",
                'file_type': 'voice',
                'file_size': 17010},
            'date': 1614373759.0})

    def test_forward_from_messages(self):
        forward_message_1 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from": {
                "id": 1325615272,
                "is_bot": True,
                "first_name": "Kraken",
                "username": "krken_bot"
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        forward_class_1 = DataSync('text', forward_message_1)

        self.assertEqual(forward_class_1.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "krken_bot",
                                 "title": "Kraken",
                                 "type": "bot"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})

        forward_message_2 = {"message_id": 525,
                             "author_signature": "Gree",
                             "sender_chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "date": datetime.fromtimestamp(1618093971),
                             "forward_from": {
                                 "id": 839534881,
                                 "is_bot": False,
                                 "first_name": "Gree",
                                 "language_code": "ru"
                             },
                             "forward_date": 1617980533,
                             "text": "Ахмад ти"
                             }

        forward_class_2 = DataSync('text', forward_message_2)
        self.assertEqual(forward_class_2.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 525,
                             "author_nickname": "Gree",
                             "text": "Ахмад ти",
                             "date": 1618093971.0,
                             "forward_from": {
                                 "id": 839534881,
                                 'username': '',
                                 "title": "Gree",
                                 "type": "user"
                             }
                         })

        forward_message_3 = {"message_id": 525,
                             "author_signature": "Gree",
                             "sender_chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "date": datetime.fromtimestamp(1618093971),
                             "forward_from": {
                                 "id": 839534881,
                                 "is_bot": False,
                                 "first_name": "Gree",
                                 "last_name": 'S2pac',
                                 "language_code": "ru"
                             },
                             "forward_date": 1617980533,
                             "text": "Ахма"
                             }

        forward_class_3 = DataSync('text', forward_message_3)
        self.assertEqual(forward_class_3.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 525,
                             "author_nickname": "Gree",
                             "text": "Ахма",
                             "date": 1618093971.0,
                             "forward_from": {
                                 "id": 839534881,
                                 'username': '',
                                 "title": "Gree S2pac",
                                 "type": "user"
                             }
                         })

    def test_forward_from_chat(self):
        forward_from_chat_1 = {
            "message_id": 526,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "date": datetime.fromtimestamp(1618095537),
            "forward_from_chat": {
                "id": -1001003313758,
                "title": "Новости Москвы",
                "username": "moscowmap",
                "type": "channel"
            },
            "forward_from_message_id": 34559,
            "forward_date": 1618063838,
            "photo": [
                {
                    "file_id": "AgACAgIAAx0CRUd1QwACAg5gci2xDhQ7MqsTIcOkjWzPV-3-",
                    "file_unique_id": "AQADVJmAny4AA9zcAgAB",
                    "file_size": 38625,
                    "width": 320,
                    "height": 320
                },
                {
                    "file_id": "AgACAgIAAx0CRUd1QwACAg5gci2xDhQ7MqsTIcOkjWzPV-3-",
                    "file_unique_id": "AQADVJmAny4AA9rcAgAB",
                    "file_size": 175448,
                    "width": 1023,
                    "height": 1023
                },
                {
                    "file_id": "AgACAgIAAx0CRUd1QwACAg5gci2xDhQ7MqsTIcOkjWzPV-3-tgAC_LYxGzYg",
                    "file_unique_id": "AQADVJmAny4AA93cAgAB",
                    "file_size": 181744,
                    "width": 800,
                    "height": 800
                }
            ],
            "caption": "🐕 В Artplay сегодня Pet-friendly Day — день,"
                       " когда посетителей пускают с животными на все выставки."
        }

        forward_class_1 = DataSync('photo', forward_from_chat_1)
        self.assertEqual(forward_class_1.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "channel"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 526,
                             "author_nickname": "Gree",
                             "text": "🐕 В Artplay сегодня Pet-friendly Day — день,"
                                     " когда посетителей пускают с животными на все выставки.",
                             "date": 1618095537.0,
                             "file": {
                                 "file_id": "AgACAgIAAx0CRUd1QwACAg5gci2xDhQ7MqsTIcOkjWzPV-3-tgAC_LYxGzYg",
                                 "file_type": "photo",
                                 "file_size": 181744
                             },
                             "forward_from": {
                                 "id": -1001003313758,
                                 "title": "Новости Москвы",
                                 "username": "moscowmap",
                                 "type": "channel"
                             },
                         })

        message_1 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "type": "private"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from_chat": {
                "id": 1325615272,
                "first_name": "Kraken",
                "type": 'channel'
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        msg_class_1 = DataSync('text', message_1)
        self.assertEqual(msg_class_1.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "",
                                 "type": "private"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "",
                                 "title": "Kraken",
                                 "type": "channel"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})

        message_2 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "first_name": "test_vega_bot",
                "type": "private"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from_chat": {
                "id": 1325615272,
                "first_name": "Kraken",
                "last_name": "Kraken",
                "username": "krken_bot",
                "type": 'channel'
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        msg_class_2 = DataSync('text', message_2)
        self.assertEqual(msg_class_2.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "private"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "krken_bot",
                                 "title": "Kraken Kraken",
                                 "type": "channel"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})

        message_3 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "type": "private"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from_chat": {
                "id": 1325615272,
                "type": 'private'
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        msg_class_1 = DataSync('text', message_3)
        self.assertEqual(msg_class_1.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "",
                                 "type": "private"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "",
                                 "title": "",
                                 "type": "private"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})

    def test_incorrect_message_from_webhook(self):
        msg1 = {'error': "incorrect webhook"}

        msg1_class = DataSync('text', msg1)
        self.assertEqual(msg1_class.to_dict(),
                         {"channel_id": 0,
                          "error": "Message parser error, please check logs"})

    def test_chat_info_handler(self):
        message_1 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "type": "private"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from": {
                "id": 1325615272,
                "is_bot": True,
                "first_name": "Kraken",
                "username": "krken_bot"
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        msg_class_1 = DataSync('text', message_1)
        self.assertEqual(msg_class_1.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "",
                                 "type": "private"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "krken_bot",
                                 "title": "Kraken",
                                 "type": "bot"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})

        message_2 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "first_name": "test_vega_bot",
                "type": "private"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from": {
                "id": 1325615272,
                "is_bot": True,
                "first_name": "Kraken",
                "username": "krken_bot"
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        msg_class_2 = DataSync('text', message_2)
        self.assertEqual(msg_class_2.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot",
                                 "type": "private"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "krken_bot",
                                 "title": "Kraken",
                                 "type": "bot"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})

        message_3 = {
            "message_id": 521,
            "author_signature": "Gree",
            "sender_chat": {
                "id": -1001162310979,
                "title": "test_vega_bot",
                "type": "channel"
            },
            "chat": {
                "id": -1001162310979,
                "first_name": "test_vega_bot",
                "last_name": "S2pac",
                "type": "private"
            },
            "date": datetime.fromtimestamp(1618088524),
            "forward_from": {
                "id": 1325615272,
                "is_bot": True,
                "first_name": "Kraken",
                "username": "krken_bot"
            },
            "forward_date": 1601715869,
            "text": "Привет, жду твой код авторизации"
        }

        msg_class_3 = DataSync('text', message_3)
        self.assertEqual(msg_class_3.to_dict(),
                         {
                             "chat": {
                                 "id": -1001162310979,
                                 "title": "test_vega_bot S2pac",
                                 "type": "private"
                             },
                             "edited": False,
                             "pinned_message": False,
                             "message_id": 521,
                             "forward_from": {
                                 "id": 1325615272,
                                 "username": "krken_bot",
                                 "title": "Kraken",
                                 "type": "bot"
                             },
                             "author_nickname": "Gree",
                             "text": "Привет, жду твой код авторизации",
                             "date": 1618088524.0})


if __name__ == '__main__':
    unittest.main()
