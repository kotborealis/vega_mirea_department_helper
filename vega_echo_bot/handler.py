"""
TG channel posts handler

Output data description:
chat - channel description in the dictionary format:
        {'id': channel id,
        'title': channel title,
        'type': type - channel
        }

pinned_message - was the post pinned / stays false if the pinned message was edited
edited - was the post edited / stays false if the edited message was pinned

message_id - tg post id. In the case of editing/pinning - the id of the message,
        not the operation itself. In other words - id does not change when editing or pinning

author_nickname - OPTIONAL - post creator nickname
text - OPTIONAL - text in the post
date - date in numeric format (unix timestamp)

file - OPTIONAL - file description in the dictionary format:
                {'file_id': file id
                'file_type': one of the supported formats
                'file_size': file size
                'file_name': file name - OPTIONAL
                }

forward_from - OPTIONAL - sender description in the dictionary format:
                {'id': sender chat/user/bot id,
                 'username': username, for private chat, supergroup, user, bot and channel if available,
                 'type': private/group/supergroup/channel/user/bot,
                 'title': sender title - for supergroups, channels and group chats/
                          first_name + last_name - for other party in a private chat or user/bot}

Note:
    A crutch is used to catch errors: self.error_happened - bool flag,
    which represents an error during message parsing. So, if you would like
    to add extra functions to class, have to consider that not all values could be initialized.
"""
import json
import logging

from datetime import datetime
from aiogram.types import Message
from varname import nameof

supported_formats = ('text', 'document', 'photo', 'voice')


class DataSync:
    """TG channel posts processing"""

    def __init__(self, msg_type: str, message: Message):
        self.logger = logging.getLogger('echo_bot.handler.DataSync')
        self.error_happened: bool = False  # crutch for error processing

        # 0 represents an error with input message:
        # structure doesn't confirm current telegram bot api
        self.channel_id: int = 0

        try:
            self.channel_id = int(message['chat']['id'])
            self._chat: dict = self._chat_info_handler(message['chat'], self.channel_id)

            self._pinned_message: bool = bool('pinned_message' in message)
            # We can pin edited messages too, so, for the difference, have to leave them without edit mark
            self._edited: bool = bool(('edit_date' in message) and not self._pinned_message)

            date: datetime = message['date'] if not self._edited else message['edit_date']
            self._date: float = date.timestamp()

            # Here we start our big crutch of pinned message. Bot takes message in such view:
            # {"message_id": ,"date": 1614332818, .....
            # "pinned_message": {"message_id": 133, "author_signature": "",
            #          		     "date": 1614298692, "edit_date": 1614298696, .....
            #          		     "document": {"file_name": "", "mime_type": "", "file_id": "", ....}
            #          		     "caption": ""}}
            validate_message: Message = message['pinned_message'] if self._pinned_message else message

            self._forward_info: dict = {}
            if 'forward_from' in validate_message:
                self._forward_info = self._forward_from_handler(validate_message['forward_from'])
            elif 'forward_from_chat' in validate_message:
                self._forward_info = self._forward_from_chat_handler(validate_message['forward_from_chat'])

            self._message_id: int = int(validate_message['message_id'])
            self._author_nickname: str = validate_message['author_signature'] if \
                'author_signature' in validate_message else ''

            # Text in message
            if msg_type != 'text':
                # Message with photo, document.. may not contain a description
                self._text: str = validate_message['caption'] if 'caption' in validate_message else ''
            else:
                # Text in 'text' message always will exist
                self._text: str = validate_message['text']

            # File description:
            self._file = self._file_info_handler(validate_message, msg_type)

        except Exception as exc:
            # Something bad has happened during message parsing, return error msg
            self.logger.error('DataSync initialization error: %s. Channel id: %s',
                              repr(exc), self.channel_id)

            self.error_happened = True

    @classmethod
    def from_pinned_msg(cls, msg: Message):
        """
        Class constructor for pinned message, which type is unknown.
        If type not included in supported formats - returns None

        :param msg: channel post description

        :returns: class instance
        """
        file_type: str = cls._get_file_type_for_pinned_msg(msg['pinned_message'])

        if len(file_type) == 0:
            # Unsupported file type
            return None
        return cls(file_type, msg)

    @staticmethod
    def _get_file_type_for_pinned_msg(message: Message) -> str:
        """
        Gets file type from pinned message, if it's in supported formats.
        Otherwise - returns an empty str.

        :param message: message block included under the heading 'pinned_message'

        :return: message type from supported formats
        """
        for msg_type in supported_formats:
            if msg_type in message:
                return msg_type

        # Unsupported file type
        return ''

    @staticmethod
    def _file_info_handler(message: Message, msg_type: str) -> dict:
        """
        Gets file info from message. If no files were attached - {}

        :param message: channel post description
        :param msg_type: one of supported types

        :return: file description in format:
                {'file_id': ...,
                'file_type': ...,
                'file_size': ...,
                'file_name': ... - OPTIONAL
                }
        """
        if msg_type == 'text':
            return {}

        if msg_type == 'photo':
            # "photo": [{"file_id": "", "file_unique_id": "", "file_size": 9493, "width": 102, "height": 320},
            #           {"file_id": "", "file_unique_id": "": 30925, "width": 231, "height": 722}]}
            # it's the same file, but in different sizes
            # Now we're looking only for an actual file
            file_desc: dict = message['photo'][len(message['photo']) - 1]
        else:
            file_desc: dict = message[msg_type]

        file_info: dict = {'file_id': file_desc['file_id'],
                           'file_type': msg_type,
                           'file_size': int(file_desc['file_size'])
                           }

        if msg_type == 'document':
            file_info['file_name'] = file_desc['file_name']

        return file_info

    @staticmethod
    def _forward_from_handler(forward_from: dict) -> dict:
        """
        Gets info from 'forward_from' field

        :param forward_from: information about original message sender

        :return: {'id': sender id,
                  'username': sender username,
                  'type': bot/user,
                  'title': sender first_name + last_name}
        """
        # According to the tg bot api documentation forward_from field is based on User subclass
        username: str = forward_from['username'] if 'username' in forward_from else ''

        # first_name is an obligatory parameter, unlike last_name, which is optional
        title: str = forward_from['first_name'] + (
            ' ' + forward_from['last_name'] if 'last_name' in forward_from else '')
        sender_type: str = 'bot' if forward_from['is_bot'] else 'user'

        return {'id': int(forward_from['id']),
                nameof(username): username,
                nameof(title): title,
                'type': sender_type}

    @staticmethod
    def _forward_from_chat_handler(forward_from_chat: dict) -> dict:
        """
        Gets info from 'forward_from_chat' field.

        :param forward_from_chat: information about messages,
         forwarded from channels or from anonymous administrators,
          information about the original sender chat.

        :return: {'id': sender chat id,
                  'username': username, for private chats, supergroups and channels if available,
                  'type': private/group/supergroup/channel,
                  'title': sender title - for supergroups, channels and group chats/
                           first_name + last_name - for other party in a private chat}
        """
        # According to the tg bot api documentation forward_from field is based on Chat subclass
        username: str = forward_from_chat['username'] if 'username' in forward_from_chat else ''

        title: str = ''
        if 'title' in forward_from_chat:
            title = forward_from_chat['title']
        elif 'first_name' in forward_from_chat:
            title = forward_from_chat['first_name'] + (
                ' ' + forward_from_chat['last_name'] if 'last_name' in forward_from_chat else '')

        return {'id': int(forward_from_chat['id']),
                nameof(username): username,
                nameof(title): title,
                'type': forward_from_chat['type']}

    @staticmethod
    def _chat_info_handler(chat: dict, channel_id: int) -> dict:
        """
        Gets info about the chat, where the message has been sent.

        :param chat: chat description
        :param channel_id: channel id
        """
        title = ''
        if 'title' in chat:
            title = chat[nameof(title)]
        elif 'first_name' in chat:
            # For private chats
            title = chat['first_name'] + (
                ' ' + chat['last_name'] if 'last_name' in chat else '')

        return {'id': channel_id,
                'title': title,
                'type': chat['type']
                }

    @staticmethod
    def write_json(text, filename='json_dump/future_rest.json'):
        """
        Standard json file writer

        :param text: data for recording
        :param filename: where to write
        """
        with open(filename, 'a', encoding='utf8') as json_file:
            json.dump(text, json_file)
            json_file.write('\n')

    def to_dict(self) -> dict:
        """
        Generates a dictionary from class instance data - if there were no errors during initialization.
        Otherwise - returns an error message: {"error": "Message parser error, please check logs"}

        :returns: no errors during init:
                     {'chat': ...,
                      'edited': ...,
                      'pinned_message': ..,
                      'message_id': ...,
                      'author_nickname': .., - OPTIONAL
                      'text': ..., - OPTIONAL
                      'file': .., - OPTIONAL
                      'date': ...,
                      'forward_from': ... - OPTIONAL
                      }
        """
        if self.error_happened:
            return {"channel_id": self.channel_id,
                    "error": "Message parser error, please check logs"}

        data_sync = {'chat': self._chat,
                     'edited': self._edited,
                     'pinned_message': self._pinned_message,
                     'message_id': self._message_id,  # doesn't change after message editing
                     'date': self._date
                     }

        # Optional params
        if len(self._author_nickname) > 0:
            data_sync['author_nickname'] = self._author_nickname

        if len(self._text) > 0:
            data_sync['text'] = self._text

        if len(self._file) > 0:
            data_sync['file'] = self._file

        if len(self._forward_info) > 0:
            data_sync['forward_from'] = self._forward_info

        return data_sync

    def error_during_initialization_check(self) -> bool:
        """
        :return: error_happened flag,
            which represents an error during initialization
        """
        return self.error_happened

    def get_channel_id(self) -> int:
        """
        :return: channel id
        """
        return self.channel_id
